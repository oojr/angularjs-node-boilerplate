'use strict';

angular.module('app.main', ['ui.router'])
 .config(function($stateProvider, $urlRouterProvider, $locationProvider){
    $stateProvider
      .state('main',{
        url: '/',
        templateUrl: 'templates/main.html',
        controller: 'MainCtrl',
      });

    $urlRouterProvider.when('', '/');
    $locationProvider.html5Mode(true);

  })
  .controller('MainCtrl', function ($scope) {
  	$scope.name = "Hello World"
   
    
  })
