# node-js-sample

This is a barebones Node.js and Angular JS app using the [Express](http://expressjs.com/) framework.

## Running Locally

Asumming you have [Node.js](http://nodejs.org/) and [Heroku Toolbelt](https://toolbelt.heroku.com/) installed on your machine:

```sh
git clone https://oojr@bitbucket.org/oojr/angularjs-node-boilerplate.git
cd angularjs-node-boilerplate
npm install
bower install
foreman start
```

App should be listening on port 5000
